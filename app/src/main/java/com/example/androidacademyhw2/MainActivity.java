package com.example.androidacademyhw2;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.vkImageView).setOnClickListener((v) -> {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vkontakte://profile/91943276"));
            startActivity(intent);
        });

        findViewById(R.id.tlgImageView).setOnClickListener((v) -> {
            Intent telegram = new Intent(Intent.ACTION_VIEW, Uri.parse("https://telegram.me/admiralpavel"));
            startActivity(telegram);
        });

        findViewById(R.id.send_msg_button).setOnClickListener((v) -> {
            String email = "admiralpavel99@gmail.com";
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setType("message/rfc822");
            intent.putExtra(Intent.EXTRA_EMAIL, email);
            intent.setData(Uri.parse("mailto:"+email));
            intent.putExtra(Intent.EXTRA_TEXT, ((EditText)findViewById(R.id.editText)).getText().toString());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_FROM_BACKGROUND);
            try {

                startActivity(intent);
            } catch (android.content.ActivityNotFoundException e) {
                Toast toast = Toast.makeText(getApplicationContext(),"Please install email app",Toast.LENGTH_SHORT);
                toast.show();

            }
        });
    }
}
